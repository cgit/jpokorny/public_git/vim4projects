Shared collection of vim plugins useful for development
=======================================================

Script ./init.sh provides a way to get selected vim plugins
useful for development.  Plugins are categorized, for list of
them see "./init.sh help" ("all" will select them all).
The plugins are prepared in this directory, in own subdirs.

To use plugins conveniently from here, install pathogen [1]
as per instructions, but add this to your ~/.vimrc:

    filetype off

    call pathogen#infect('~/path/to/this/repo_dir')
    call pathogen#helptags()

    filetype plugin indent on
    syntax on

Alternatively, clone vim4projects.git repo or symlink already
cloned location to ~/.vim/bundle.  You can also copy the content
here directly, but you will miss automatical updates and
additions of new plugins via git pull.  For all these three
alternatives, call pathogen without the parameter instead.
Indeed, you can have more "pathogen#infect" lines
(e.g., without argument for your basic plugins), just
keep in mind the order matters and conficts may arise.


To quickstart using the plugins, you can add this to ~/.vimrc:

    " vim: always use mouse (shift may be needed for copy/paste)
    set mouse=a
    " Tagbar: toggle Tagbar window with F8
    nmap <F8> :TagbarToggle<CR>


After restarting vim, everything should work.  To check this
(if ./init.sh python or ./init.sh all was used only),
":PymodeVersion'" should output something like:

    Current python-mode version: 0.5.5"

It is important that this test is passing!


To remove particular category completely, just run
"./init.sh del <category>".


Please note that some of the plugins have dependencies;
non-exhaustive enumeration (refer to per-plugin README files):

- Tagbar
  - exuberant ctags (?), Fedora: ctags-etags
  - jsctags + node.js: see respective README


For configuration of the plugins, you may be interested in
following sources (mostly the main pages at github.com):

- Python-mode: https://github.com/klen/python-mode

The settings that should be shared can be set in _vimrc_local.vim
files within particular project repo (provided that contained
local_vimrc plugin is used).


If you think some plugin is missing, please drop me a line [2].
Note that this collection is meant to provide only plugins
that support collaboration (as local_vimrc) and/or are a clear
choice for selected kind of development (Python, TG2, web, ...).

Plugins of deliberate personal choice and possibly conflicting
what another one uses privately (e.g., buffer management) does
not belong here.  If there is a conflict of included plugin,
please let me know.  Tagbar is an exception, alternative is
TagList, but it seems unmaintained, and the plugin no doubt
increases productivity, providing the source file overview
and its in "optional" category anyway.


See also [3] containing some more plugins and customization
(amongst other dotfiles).


[1] https://github.com/tpope/vim-pathogen
[2] mailto:jpokorny@redhat.com
[3] http://fedorapeople.org/cgit/jpokorny/public_git/dotfiles.git
